"""
Name of the project : RegEx
File name : python_2.py
Description : program that matches a string that has an a followed by zero or
              more b's.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_2.py
input : Hello ab
Output : yes there is
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[/w+a/w+]",user_input)
if x :
    print("yes there is")
else :
    print("there is no such strings")
