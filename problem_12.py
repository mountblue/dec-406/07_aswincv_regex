"""
Name of the project : RegEx
File name : python_12.py
Description : searches for z in a string
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_12.py
input : zero
Output : yes
"""


import re
user_input1 = input("enter the string  :  ")
x = re.search("[z]",user_input1)
if x :
    print("yes")
else :
    print("false")
