"""
Name of the project : RegEx
File name : python_6.py
Description : program that matches a string that has an a followed by two to
              three 'b'.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_6.py
input : abbb
Output : false
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[abb/w+]",user_input)
y = re.search("[abbbb]",user_input)
if x and y :
    print("false")
else :
    print("yes")
