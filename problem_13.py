"""
Name of the project : RegEx
File name : python_13.py
Description : Python program that matches a word containing 'z', not start or
              end of the word.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_14.py
input : zero
Output : false
"""


import re
user_input1 = input("enter the string  :  ")
x = re.search("\Bz\B",user_input1)
if x :
    print(x.group())
else :
    print("false")
