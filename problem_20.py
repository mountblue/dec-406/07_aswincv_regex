"""
Name of the project : RegEx
File name : python_20.py
Description :  program to search a literals string in a string and also find
               the location within the original string where the pattern occurs.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_20.py
input : hello hai my name is aswin  1  aswin
Output :  15  20
"""


import re
user_input1 = input("enter the string  :  ")
num = input("enter the number of words to be searched  :  ")
for i in range(0,int(num)) :
    user_input2 = input("enter the word  :  ")
    x = re.search(user_input2,user_input1)
    print(x.span())
