"""
Name of the project : RegEx
File name : python_4.py
Description : program that matches a string that has an a followed by zero or
              one 'b'.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_4.py
input : abb
Output : false
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[a/w+]",user_input)
y = re.search("[^abb/w+]",user_input)
if x and y :
    print("true")
    print(y.span())
else :
    print("false")
