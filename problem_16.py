"""
Name of the project : RegEx
File name : python_16.py
Description : program to remove leading zeros from an IP address
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_16.py
input : 128.000.99.5
Output : 128..99.5
"""


import re
user_input1 = input("enter the ipaddress  :  ")
x = re.sub("0","",user_input1)
if x :
    print(x)
else :
    print("false")
