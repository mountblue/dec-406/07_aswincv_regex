"""
Name of the project : RegEx
File name : python_9.py
Description : Python program that matches a string that has an 'a' followed by
              anything, ending in 'b'.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_9.py
input : amb
Output : yes
"""


import re
user_input = input("enter the string  :  ")
x = re.search("a.*?b$",user_input)
if x :
    print("yes")
else :
    print("false")
