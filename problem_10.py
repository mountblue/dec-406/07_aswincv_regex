"""
Name of the project : RegEx
File name : python_9.py
Description : Python that searches for a string at the begining of a string
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_9.py
input : hello dear
Output : hello
"""


import re
user_input1 = input("enter the string  :  ")
user_input2 = input("enter the string to be searched  :  ")
user_input2 = "^" + user_input2 + ".*"
x = re.search(user_input2,user_input1)
if x :
    print("yes")
else :
    print("false")
