"""
Name of the project : RegEx
File name : python_3.py
Description : program that matches a string that has an a followed by one or
              more b's.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_3.py
input : Hello ab
Output : yes there is
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[ab/w+]",user_input)
if x :
    print("yes there is")
else :
    print("there is no such strings")
