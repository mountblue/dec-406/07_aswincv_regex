"""
Name of the project : RegEx
File name : python_7.py
Description : program to find sequences of lowercase letters joined with a
              underscore.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_7.py
input : a_b
Output : 0 2
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[a-z]_",user_input)
if x :
    print(x.span())
else :
    print("false")
