"""
Name of the project : RegEx
File name : python_15.py
Description :  program where a string will start with a specific number.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_15.py
input : 1hello 2hai 3hey
Output : 2hai
"""


import re
user_input1 = input("enter the string  :  ")
user_input2 = input("enter the number  :  ")
user_input2 += "\w+"
x = re.search(user_input2,user_input1)
if x :
    print(x.group())
else :
    print("false")
