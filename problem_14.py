"""
Name of the project : RegEx
File name : python_14.py
Description : Print the word which contain capital letters only
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_14.py
input : hello DEAR hai
Output : DEAR
"""


import re
user_input1 = input("enter the string  :  ")
x = re.search("[A-Z]\w+",user_input1)
if x :
    print(x.group())
else :
    print("false")
