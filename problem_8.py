"""
Name of the project : RegEx
File name : python_8.py
Description : Python program to find sequences of one upper case letter
              followed by lower case letters.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_8.py
input : Hello
Output : yes
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[^a-z]+[a-z]",user_input)
if x :
    print("yes")
else :
    print("false")
