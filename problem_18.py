"""
Name of the project : RegEx
File name : python_18.py
Description :  Python program to search the numbers (0-9) of length between 1
               to 3 in a given string.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_18.py
input : hello 123
Output :  yes
"""


import re
user_input1 = input("enter the string  :  ")
x = re.findall("[1-9]{1,3}",user_input1)
if x :
    print("yes")
else :
    print("false")
