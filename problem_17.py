"""
Name of the project : RegEx
File name : python_17.py
Description : program to check for a number at the end of a string.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_17.py
input : hello its me2   2
Output :  yes
"""


import re
user_input1 = input("enter the string  :  ")
user_input2 = input("enter the number to be searched  :  ")
user_input2 = ".*"+user_input2
x = re.search(user_input2,user_input1)
if x :
    print("yes")
else :
    print("false")
