"""
Name of the project : RegEx
File name : python_11.py
Description : program that matches a word at end of string, with optional
              punctuation.
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_11.py
input : hello dear
Output : hello
"""


import re
user_input1 = input("enter the string  :  ")
user_input2 = input("enter the string to be searched  :  ")
user_input2 += ".$"
x = re.search(user_input2,user_input1)
if x :
    print("yes")
else :
    print("false")
