"""
Name of the project : RegEx
File name : python_5.py
Description : program that matches a string that has an a followed by three 'b'
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_5.py
input : Hello abbb
Output : yes there is
"""


import re
user_input = input("enter the string  :  ")
x = re.search("[abbb/w+]",user_input)
if x :
    print("yes there is")
else :
    print("there is no such strings")
