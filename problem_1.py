"""
Name of the project : RegEx
File name : python_1.py
Description : program to check that a string contains only a certain set of
              characters (in this case a-z, A-Z and 0-9)
Author : Aswin cv
Contact : Aswin.CV@mountblue.io
Command : python3  problem_1.py
input : Hello123
Output : true
"""


import re
user_input = input("enter the string   :  ")
x = re.search("[^a-zA-Z0-9]",user_input)
if x :
    print("it contains other characters also\n")
else :
    print("true")
